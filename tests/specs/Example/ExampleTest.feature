Feature: A basic test to prove testcafe is up and running

Background: 
    Given I navigate to the testcafe example page 

    Scenario: A basic test using the testcafe API
        When I enter "Test Name" into the developer name input box
        And I select the submit button
        Then I should see the text "Thank you," + "<name>"
        