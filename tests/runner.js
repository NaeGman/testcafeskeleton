const createTestCafe = require('gherkin-testcafe');

module.exports = async () => {
    const testcafe = await createTestCafe();
    const runner = await testcafe.createRunner();
    const remoteConnection = await testcafe.createBrowserConnection();

    return runner
        .src(['steps/*.js', 'tests/**/*.feature]'])
        .browsers([remoteConnection, 'chrome'])
        .run();
};